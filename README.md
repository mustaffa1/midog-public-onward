# MIDOG

Mitosis Domain Generalisation
https://imi.thi.de/midog/

# Preprint Arxiv Identifier
submit/3910782

## Objective

- The purpose is the detect mitosis across slides scanned using different scanners at different locations. The main underlying problem is to address shift in staining domains and then design an algo to localise mitosis accurately.



## Branches info
Please keep 'main' branch empty till end.
- [ ] Data_process : has the base codes for dealing with data. patch extaraction,preprocessing, annotations and mask generations etc. 
- [ ] expreminets : codes for segmentations,detections, classifications used.
- [ ] preliminary_test_phase : final branch with docker image for testing. The branch is be used for submission.



## Authors and acknowledgment
We were inspired from and used code from:
- [Cycle GAN](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix)
- [Detectron2](https://github.com/facebookresearch/detectron2)


## Project status
- The project is in Preliminary Test Phase.

